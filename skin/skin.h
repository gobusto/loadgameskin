/**
@file
@brief Import game-specific image formats and export them to common filetypes.

Copyright (C) 2016 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef __QQQ_SKIN_H__
#define __QQQ_SKIN_H__

#ifdef __cplusplus
extern "C" {
#endif

#define SKIN_MAXNAME 64 /**< @brief Size of a skin name string in bytes. */

typedef unsigned long skin_count_t; /**< @brief Size of the "skin set" count. */
typedef unsigned long skin_size_t;  /**< @brief Size of width/height values.  */
typedef unsigned char skin_bits_t;  /**< @brief Size of bits-per-pixel value. */
typedef unsigned char skin_data_t;  /**< @brief Size of pixel R/G/B/A values. */
typedef unsigned char skin_flag_t;  /**< @brief Size of export flags bitmaps. */

#define SKIN_FLAG_COMPRESS 0x01 /**< @brief Compress the output, if possible. */
#define SKIN_FLAG_NOTUSED1 0x02 /**< @brief Not used yet.                     */
#define SKIN_FLAG_NOTUSED2 0x04 /**< @brief Not used yet.                     */
#define SKIN_FLAG_NOTUSED3 0x08 /**< @brief Not used yet.                     */
#define SKIN_FLAG_NOTUSED4 0x10 /**< @brief Not used yet.                     */
#define SKIN_FLAG_NOTUSED5 0x20 /**< @brief Not used yet.                     */
#define SKIN_FLAG_NOTUSED6 0x40 /**< @brief Not used yet.                     */
#define SKIN_FLAG_NOTUSED7 0x80 /**< @brief Not used yet.                     */

/**
@brief Enumerates possible error codes returned by file export functions.
*/

typedef enum
{

  SKIN_STATUS_NULL, /**< @brief A null pointer was given, but not expected. */
  SKIN_STATUS_FILE, /**< @brief A file could not be opened or read/updated. */
  SKIN_STATUS_SIZE, /**< @brief The image is too large for the format used. */
  SKIN_STATUS_OKAY  /**< @brief No problems occurred; everything went okay. */

} skin_status_t;

/**
@brief Enumerates possible image export formats.
*/

typedef enum
{

  SKIN_FORMAT_BMP,  /**< @brief Windows or OS/2 BitMaP. */
  SKIN_FORMAT_PCX,  /**< @brief ZSoft PiCture eXchange. */
  SKIN_FORMAT_PNM,  /**< @brief Portable aNyMap bitmap. */
  SKIN_FORMAT_TGA   /**< @brief Truevision TarGA image. */

} skin_format_t;

/**
@brief This structure represents a single image within a group of images.

The bit-depth indicates the type of data being stored:

+ Greyscale (luminance) data is stored as an 8-bit image: `LLLL...`
+ Greyscale + alpha data is stored as a 16-bit image: `LALALALA...`
+ RGB data is stored as a 24-bit image: `RGBRGBRGBRGB...`
+ RGBA data is stored as a 32-bit image: `RGBARGBARGBARGBA...`

Pixel data is stored as a continuous stream of pixel values, with no padding to
align rows to n-byte boundaries.
*/

typedef struct
{

  char name[SKIN_MAXNAME];  /**< @brief Name of the image; may be blank. */

  skin_size_t w;      /**< @brief Width of this image in pixels.           */
  skin_size_t h;      /**< @brief Height of this image in pixels.          */
  skin_bits_t bpp;    /**< @brief Bits-per-pixel; may be 8, 16, 24, or 32. */
  skin_data_t *data;  /**< @brief Pointer to the actual pixel data.        */

} skin_s;

/**
@brief This structure represents a group of one or more images.

Games often store more than one image in a single file. Some examples:

+ Smaller mipmap versions of a "main" image.
+ Different textures for various parts of a single 3D model.
+ Alternative skins, such as different costumes for the same character model.
+ Frames of animation for sprites or animated textures.

As such, the various file-loading functions return a *group* of textures rather
than a single image. However, the file-saving functions work with a single skin
so that individual images can be extracted without dumping an entire set.
*/

typedef struct
{

  skin_count_t num_items; /**< @brief Number of images stored by this group. */
  skin_s       **item;    /**< @brief Points to an array of skin structures. */

} skin_set_s;

/**
@brief Create a new (empty) group of images.

@param num_items The number of "slots" to allocate.
@return A valid pointer on success, or NULL on failure.
*/

skin_set_s *skinSetCreate(skin_count_t num_items);

/**
@brief Destroy a previously-created image group.

Any images within the group are destroyed too.

@param skin_set The image group to be destroyed.
*/

void skinSetDelete(skin_set_s *skin_set);

/**
@brief Create a new (blank) image structure.

@param w Width of this image in pixels.
@param h Height of this image in pixels.
@param bpp Bits-per-pixel; may be 8, 16, 24, or 32.
@param name A "name" for the image; this is optional, and may be NULL.
@return A valid pointer on success, or NULL on failure.
*/

skin_s *skinCreate(skin_size_t w, skin_size_t h, skin_bits_t bpp, const char *name);

/**
@brief Destroy a previously-created image.

@param skin The image to be destroyed.
*/

void skinDelete(skin_s *skin);

/**
@brief Load an image set from an in-memory data buffer.

@param length The length of the data buffer, in bytes.
@param data A raw data buffer.
@param palette Optional: A palette to use for images without an "internal" one.
@return A valid pointer on success, or NULL on failure.
*/

skin_set_s *skinSetLoadData(long length, unsigned char *data, const skin_s *palette);

/**
@brief Load an image set from a file.

This calls `skinSetLoadData()` internally.

@param name The name (and path) of the file to be loaded.
@param palette Optional: A palette to use for images without an "internal" one.
@return A valid pointer on success, or NULL on failure.
*/

skin_set_s *skinSetLoadFile(const char *name, const skin_s *palette);

/**
@brief Load a palette from an in-memory data buffer.

Only 256 x 24-bit palettes are currently supported, so the data must be exactly
768 bytes long.

@param length The length of the data buffer, in bytes.
@param data A raw data buffer.
@return A valid pointer on success, or NULL on failure.
*/

skin_s *skinPaletteLoadData(long length, unsigned char *data);

/**
@brief Load a palette from a file.

This calls `skinPaletteLoadData()` internally.

@param name The name (and path) of the file to be loaded.
@return A valid pointer on success, or NULL on failure.
*/

skin_s *skinPaletteLoadFile(const char *name);

/**
@brief Save a single image to a file.

@param skin The image to be saved.
@param name The filename to be used.
@param format The image format to use when exporting.
@param flags Any special options, such as `SKIN_FLAG_COMPRESS`, if applicable.
@return `SKIN_STATUS_OKAY` if everything went OK, or a suitable error if not.
*/

skin_status_t skinSaveFile(const skin_s *skin, const char *name, skin_format_t format, skin_flag_t flags);

#ifdef __cplusplus
}
#endif

#endif /* __QQQ_SKIN_H__ */
