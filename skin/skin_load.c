/**
@file
@brief Provides functions for importing images from files.

Copyright (C) 2016 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "skin.h"

/* AGI-related constants: */

#define SKIN_AGI_4CC       0x00000020 /**< @brief about */
#define SKIN_AGI_HEAD_SIZE 48         /**< @brief about */

/* ETI-related constants: */

#define SKIN_ETI_4CC          20          /**< @brief about */
#define SKIN_ETI_HEAD_SIZE    16          /**< @brief about */
#define SKIN_ETI_FLAG_PALETTE 0x00000001  /**< @brief about */

/* PEG-related constants: */

#define SKIN_PEG_HEAD_SIZE 32 /**< @brief Size of the PEG header in bytes. */
#define SKIN_PEG_INFO_SIZE 64 /**< @brief Size of each PEG item in bytes.  */

#define SKIN_PEG_4CC 0x564b4547 /**< @brief Spells "GEKV" as ASCII text. */
#define SKIN_PEG_VER 6          /**< @brief PEG file format version.     */

/**
@brief Read a little-endian UINT16 value from a buffer.

@param x A pointer to the start of a binary data buffer.
@return The extracted value on success, or zero on failure.
*/

#define skinGetUI16_LE(x) ((unsigned short)((x) ? \
  ((unsigned short)(x)[0] << 0) + \
  ((unsigned short)(x)[1] << 8) : 0))

/**
@brief Read a little-endian UINT32 value from a buffer.

@param x A pointer to the start of a binary data buffer.
@return The extracted value on success, or zero on failure.
*/

#define skinGetUI32_LE(x) ((unsigned long)((x) ? \
  ((unsigned long)(x)[0] << 0 ) + \
  ((unsigned long)(x)[1] << 8 ) + \
  ((unsigned long)(x)[2] << 16) + \
  ((unsigned long)(x)[3] << 24) : 0))


/**
@brief Fix "wrong" palette indexes for various image formats used by PS2 games.

If we divide the palette into blocks of 32 RGBA values, then the first and last
8 colours can be used as-is. However, the second and third groups need swapping
around. In other words, if the original palette looked like this:

    0  1  2  3  4  5  6  7
    8  9  10 11 12 13 14 15
    16 17 18 19 20 21 22 23
    24 25 26 27 28 29 30 31

...then we need to use it like this:

    0  1  2  3  4  5  6  7
    16 17 18 19 20 21 22 23
    8  9  10 11 12 13 14 15
    24 25 26 27 28 29 30 31

This only applies to 256-colour palettes; 16-colour ones are fine as they are.

@param pal_index The "original" palette index, as specified by the file.
@return The "fixed" palette index.
*/

static unsigned char skinFixIndexPS2(unsigned char pal_index)
{
  unsigned char temp = pal_index % 32;
  if (temp >= 8  && temp < 16) { pal_index += 8; }
  if (temp >= 16 && temp < 24) { pal_index -= 8; }
  return pal_index;
}

/**
@brief Convert 0-to-128 alpha values into a 0-to-255 range instead.

@param alpha The "original" alpha value, as specified by the file.
@return The "fixed" alpha value.
*/

static unsigned char skinFixAlphaPS2(unsigned char alpha)
{
  return alpha < 128 ? alpha * 2 : 255;
}

/**
@brief Load a Chasm: The Rift .OBJ animated sprite image.

@param length The length of the data buffer, in bytes.
@param data A raw data buffer.
@param palette The palette, as a 1D image; This should contain 256 RGB values.
@return A valid pointer on success, or NULL on failure.
*/

static skin_set_s *skinSetLoadOBJ(long length, unsigned char *data, const skin_s *palette)
{
  skin_set_s *skin_set;
  unsigned short i;
  long offs;

  /* Make sure that there is a reasonable amount of data + the palette is OK. */
  if (length < 9 || palette->w < 256 || palette->bpp != 24) { return NULL; }

  /* Chasm .OBJ files have no header; they simply state the number of images: */
  i = skinGetUI16_LE(data);
  skin_set = skinSetCreate(i);
  if (!skin_set) { return NULL; }

  /* Attempt to load each image in turn + check to ensure this file is valid. */
  for (offs = 2, i = 0; i < skin_set->num_items; ++i)
  {
    const unsigned short h = skinGetUI16_LE(&data[offs + 0]);
    const unsigned short w = skinGetUI16_LE(&data[offs + 2]);
    unsigned long p;

    /* Each image consists of 3 UI16 values (H/W/?) and W*H palette indices: */
    if (offs + 6 + w*h > length) { skinSetDelete(skin_set); return NULL; }
    skin_set->item[i] = skinCreate(w, h, 32, NULL);
    if (!skin_set->item[i]) { skinSetDelete(skin_set); return NULL; }

    /* RGB values range from 0-63. Transparent pixels use index 255. */
    for (offs += 6, p = 0; p < (long)w*h; ++offs, ++p)
    {
      skin_set->item[i]->data[p*4 + 0] = palette->data[data[offs]*3 + 0] * 4;
      skin_set->item[i]->data[p*4 + 1] = palette->data[data[offs]*3 + 1] * 4;
      skin_set->item[i]->data[p*4 + 2] = palette->data[data[offs]*3 + 2] * 4;
      skin_set->item[i]->data[p*4 + 3] = data[offs] == 255 ? 0 : 255;
    }
  }

  return skin_set;
}

/**
@brief Load a BCV: Battle Construction Vehicles .AGI image.

@param length The length of the data buffer, in bytes.
@param data A raw data buffer.
@return A valid pointer on success, or NULL on failure.
*/

static skin_set_s *skinSetLoadAGI(long length, unsigned char *data)
{
  skin_set_s *skin_set;
  unsigned long pal_offs, pal_size, i;
  unsigned short w, h;

  /* Make sure that there is a reasonable amount of data + the header is OK: */
  if (length < SKIN_AGI_HEAD_SIZE + 1 + 4) { return NULL; }
  else if (skinGetUI32_LE(&data[0]) != SKIN_AGI_4CC) { return NULL; }
  else if (skinGetUI16_LE(&data[4]) != 1) { return NULL; }
  else if (skinGetUI16_LE(&data[6]) != 1) { return NULL; }

  /* First comes the "real" image, whose offset (should) always be the same: */
  if (skinGetUI32_LE(&data[8]) != SKIN_AGI_HEAD_SIZE) { return NULL; }
  /* Unknown UI32 */
  /* Unknown UI32 */
  /* Unknown UI32 */
  w = skinGetUI16_LE(&data[24]);
  h = skinGetUI16_LE(&data[26]);

  /* Then comes the palette "image" info: */
  pal_offs = skinGetUI32_LE(&data[28]);
  /* Unknown UI32 */
  /* Unknown UI32 */
  /* Unknown UI32 */
  pal_size = skinGetUI16_LE(&data[44]);
  pal_size *= skinGetUI16_LE(&data[46]);

  /* All images are expected to have either 16 or 256 colours: */
  if (pal_size != 16 && pal_size != 256) { return NULL; }

  /* We only need one image for this image "set": */
  skin_set = skinSetCreate(1);
  if (!skin_set) { return NULL; }
  skin_set->item[0] = skinCreate(w, h, 32, NULL);
  if (!skin_set->item[0]) { skinSetDelete(skin_set); return NULL; }

  /* Read the pixel data: */
  for (i = 0; i < (long)w*h; ++i)
  {
    unsigned char pal_id;
    unsigned long offs;

    if (pal_size == 16)
    {
      pal_id = data[SKIN_AGI_HEAD_SIZE + (i / 2)];
      if (i % 2) { pal_id = pal_id >> 4; }
      pal_id = pal_id & 0x0F;
    }
    else { pal_id = skinFixIndexPS2(data[SKIN_AGI_HEAD_SIZE + i]); }

    offs = pal_offs + (pal_id * 4);
    skin_set->item[0]->data[(i*4) + 0] = data[offs + 0];
    skin_set->item[0]->data[(i*4) + 1] = data[offs + 1];
    skin_set->item[0]->data[(i*4) + 2] = data[offs + 2];
    skin_set->item[0]->data[(i*4) + 3] = skinFixAlphaPS2(data[offs + 3]);
  }

  return skin_set;
}

/**
@brief Load a Robot Warlords .ETI image.

@param length The length of the data buffer, in bytes.
@param data A raw data buffer.
@return A valid pointer on success, or NULL on failure.
*/

static skin_set_s *skinSetLoadETI(long length, unsigned char *data)
{
  skin_set_s *skin_set;
  unsigned long flags, image_offs, i;
  unsigned short w, h;

  /* Make sure that there is a reasonable amount of data + the header is OK: */
  if (length < SKIN_ETI_HEAD_SIZE) { return NULL; }
  else if (skinGetUI32_LE(&data[0]) != SKIN_ETI_4CC) { return NULL; }

  /* These flags indicate whether or not a palette is used: */
  flags = skinGetUI32_LE(&data[4]);

  /* If a palette IS used, the "first" image is actually the palette: */
  if (flags & SKIN_ETI_FLAG_PALETTE)
  {
    /* The palette pretends to be a 16x16 image with 256 16-bit values: */
    image_offs = skinGetUI32_LE(&data[8]);
    if (image_offs != SKIN_ETI_HEAD_SIZE + 512) { return NULL; }
    else if (skinGetUI16_LE(&data[12]) != 16) { return NULL; }
    else if (skinGetUI16_LE(&data[14]) != 16) { return NULL; }

    /* The second, "fake" header for the "real" image has no 4CC or flags: */
    if      (skinGetUI32_LE(&data[image_offs + 0]) != 0) { return NULL; }
    else if (skinGetUI32_LE(&data[image_offs + 4]) != 0) { return NULL; }
  }
  else { image_offs = 0; }

  /* The offset of the "real" image header should indicate the expected EOF: */
  if ((long)skinGetUI32_LE(&data[image_offs + 8]) > length) { return NULL; }
  w = skinGetUI16_LE(&data[image_offs + 12]);
  h = skinGetUI16_LE(&data[image_offs + 14]);

  /* We only need one image for this image "set": */
  skin_set = skinSetCreate(1);
  if (!skin_set) { return NULL; }
  skin_set->item[0] = skinCreate(w, h, 24, NULL);
  if (!skin_set->item[0]) { skinSetDelete(skin_set); return NULL; }

  /* Read the pixel data: */
  for (i = 0; i < (long)w*h; ++i)
  {
    unsigned long offs;
    unsigned short value;

    /* Figure out where the actual RGB value lives: */
    if (flags & SKIN_ETI_FLAG_PALETTE)
      offs = skinFixIndexPS2(data[image_offs + SKIN_ETI_HEAD_SIZE + i]);
    else
      offs = i;

    /* Read the 16-bit RGB value and "decompact" it: */
    value = skinGetUI16_LE(&data[SKIN_ETI_HEAD_SIZE + (offs * 2)]);
    skin_set->item[0]->data[(i*3) + 0] = ((value >> 0 ) & 31) * 8;
    skin_set->item[0]->data[(i*3) + 1] = ((value >> 5 ) & 31) * 8;
    skin_set->item[0]->data[(i*3) + 2] = ((value >> 10) & 31) * 8;
  }

  return skin_set;
}

/**
@brief Load a Red Faction .PEG texture group.

@param length The length of the data buffer, in bytes.
@param data A raw data buffer.
@return A valid pointer on success, or NULL on failure.
*/

static skin_set_s *skinSetLoadPEG(long length, unsigned char *data)
{
  skin_set_s *skin_set;
  unsigned long len_items, len_data, num_items, i;

  /* Make sure that there is a reasonable amount of data + the header is OK. */
  if (length < SKIN_PEG_HEAD_SIZE + SKIN_PEG_INFO_SIZE + 514) { return NULL; }
  else if (skinGetUI32_LE(&data[0]) != SKIN_PEG_4CC) { return NULL; }
  else if (skinGetUI32_LE(&data[4]) != SKIN_PEG_VER) { return NULL; }
  /* Read the values we're interested in... */
  len_items = skinGetUI32_LE(&data[8 ]);
  len_data  = skinGetUI32_LE(&data[12]);
  num_items = skinGetUI32_LE(&data[16]);
  /* ...and verify that they make sense: */
  if ((long)(SKIN_PEG_HEAD_SIZE+len_items+len_data) < length) { return NULL; }
  else if (len_items != num_items * SKIN_PEG_INFO_SIZE)       { return NULL; }
  /* I'm not 100% sure what these values are, but they should be as follows: */
  else if (skinGetUI32_LE(&data[20]) != 0       ) { return NULL; }
  else if (skinGetUI32_LE(&data[24]) < num_items) { return NULL; }
  else if (skinGetUI32_LE(&data[28]) != 16      ) { return NULL; }

  /* We've verified that the header makes sense; attempt to load the images. */
  skin_set = skinSetCreate(num_items);
  if (!skin_set) { return NULL; }

  for (i = 0; i < num_items; ++i)
  {
    const unsigned long offs = SKIN_PEG_HEAD_SIZE + (i * SKIN_PEG_INFO_SIZE);

    char name[49];
    unsigned short w, h;
    unsigned char palette_type;
    unsigned long data_offs, data_end;

    unsigned char palette[1024];
    unsigned long p;

    w = skinGetUI16_LE(&data[offs + 0]);
    h = skinGetUI16_LE(&data[offs + 2]);
    /* data[offs + 4] is a UINT8 value, and should always(?) be 4 or 7. */
    palette_type = data[offs + 5];
    /* data[offs + 6 ] is a UINT16 value, and seems to be a "flags" field... */
    /* data[offs + 8 ] is a UINT16 value, and seems to be a "flags" field... */
    /* data[offs + 10] is a UINT16 value, and affects the data size somehow. */
    memset(name, 0, 49); memcpy(name, &data[offs + 12], 48);
    data_offs = skinGetUI32_LE(&data[offs + 60]);

    /* Verify that everything makes sense + attempt to allocate memory: */
    switch (palette_type)
    {
      case 0: data_end = data_offs + (w*h*4);           break;
      case 1: data_end = data_offs + (w*h*1) + (256*2); break;
      case 2: data_end = data_offs + (w*h*1) + (256*4); break;
      default: data_end = 0; break;
    }
    if (palette_type > 2 || (long)data_end > length)
    { skinSetDelete(skin_set); return NULL; }

    skin_set->item[i] = skinCreate(w, h, 32, name);
    if (!skin_set->item[i]) { skinSetDelete(skin_set); return NULL; }

    /* First, copy the palette data, which may be either 16-bit or 32-bit: */
    for (p = 0; p < 256; ++p)
    {
      if (palette_type == 1)
      {
        unsigned short value = skinGetUI16_LE(&data[data_offs + p*2]);
        palette[p*4 + 0] = ((value >> 0 ) & 31) * 8;
        palette[p*4 + 1] = ((value >> 5 ) & 31) * 8;
        palette[p*4 + 2] = ((value >> 10) & 31) * 8;
        palette[p*4 + 3] = ((value >> 15) & 1 ) * 255;
      }
      else if (palette_type == 2)
      {
        palette[p*4 + 0] = data[data_offs + p*4 + 0];
        palette[p*4 + 1] = data[data_offs + p*4 + 1];
        palette[p*4 + 2] = data[data_offs + p*4 + 2];
        palette[p*4 + 3] = data[data_offs + p*4 + 3] > 0 ? 255 : 0;
      }
    }

    /* Next, copy the actual pixel data: */
    for (p = 0; p < (long)w*h; ++p)
    {
      if (palette_type == 0)
      {
        /* Type 0 images do not have a palette; they contain raw RGBA data: */
        const unsigned long pixel = data[data_offs + p*4];
        skin_set->item[i]->data[p*4 + 0] = data[pixel*4 + 0];
        skin_set->item[i]->data[p*4 + 1] = data[pixel*4 + 1];
        skin_set->item[i]->data[p*4 + 2] = data[pixel*4 + 2];
        skin_set->item[i]->data[p*4 + 3] = data[pixel*4 + 3];
      }
      else
      {
        /* Type 1 and 2 images use a palette; they contain palette indices: */
        const int palette_index = data[data_offs + palette_type*512 + p];
        skin_set->item[i]->data[p*4 + 0] = palette[palette_index*4 + 0];
        skin_set->item[i]->data[p*4 + 1] = palette[palette_index*4 + 1];
        skin_set->item[i]->data[p*4 + 2] = palette[palette_index*4 + 2];
        skin_set->item[i]->data[p*4 + 3] = palette[palette_index*4 + 3];
      }
    }
  }

  return skin_set;
}

/*
[PUBLIC] Load an image set from an in-memory data buffer.
*/

skin_set_s *skinSetLoadData(long length, unsigned char *data, const skin_s *palette)
{
  skin_set_s *skin_set = NULL;
  if (length < 1 || !data) { return SKIN_STATUS_NULL; }

  if (palette)
  {
    skin_set = skinSetLoadOBJ(length, data, palette);
  }

  if (!skin_set) { skin_set = skinSetLoadAGI(length, data); }
  if (!skin_set) { skin_set = skinSetLoadETI(length, data); }
  if (!skin_set) { skin_set = skinSetLoadPEG(length, data); }

  return skin_set;
}

/*
[PUBLIC] Load an image set from a file.
*/

skin_set_s *skinSetLoadFile(const char *name, const skin_s *palette)
{
  skin_set_s *skin_set = NULL;
  FILE *src;
  long length;
  unsigned char *data;

  if (!name) { return NULL; }
  src = fopen(name, "rb");
  if (!src) { return NULL; }

  fseek(src, 0, SEEK_END);
  length = ftell(src);
  data = (unsigned char*)malloc(length);

  if (data)
  {
    rewind(src);
    fread(data, 1, length, src);
    skin_set = skinSetLoadData(length, data, palette);
    free(data);
  }

  fclose(src);
  return skin_set;
}

/*
[PUBLIC] Load a palette from an in-memory data buffer.
*/

skin_s *skinPaletteLoadData(long length, unsigned char *data)
{
  skin_s *palette;
  if (length != 768 || !data) { return NULL; }
  palette = skinCreate(256, 1, 24, NULL);
  if (palette) { memcpy(palette->data, data, 768); }
  return palette;
}

/*
[PUBLIC] Load a palette from a file.
*/

skin_s *skinPaletteLoadFile(const char *name)
{
  skin_s *palette = NULL;
  FILE *src;
  long length;
  unsigned char *data;

  if (!name) { return NULL; }
  src = fopen(name, "rb");
  if (!src) { return NULL; }

  fseek(src, 0, SEEK_END);
  length = ftell(src);
  data = (unsigned char*)malloc(length);

  if (data)
  {
    rewind(src);
    fread(data, 1, length, src);
    palette = skinPaletteLoadData(length, data);
    free(data);
  }

  fclose(src);
  return palette;
}
