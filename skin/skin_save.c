/**
@file
@brief Provides functions for exporting images as files.

Copyright (C) 2016 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "skin.h"

/* PNM-related constants: */

#define SKIN_PNM_TYPE_MONO     1  /**< @brief Black-and-white ASCII images.  */
#define SKIN_PNM_TYPE_GREY     2  /**< @brief Greyscale ASCII images.        */
#define SKIN_PNM_TYPE_RGBA     3  /**< @brief RGB ASCII images.              */
#define SKIN_PNM_TYPE_MONO_BIN 4  /**< @brief Black-and-white binary images. */
#define SKIN_PNM_TYPE_GREY_BIN 5  /**< @brief Greyscale binary images.       */
#define SKIN_PNM_TYPE_RGBA_BIN 6  /**< @brief RGB binary images.             */

/* TGA-related constants (NOTE: Types 32 and 33 are omitted from this list): */

#define SKIN_TGA_TYPE_CMAP     1  /**< @brief Uncompressed, colourmap image. */
#define SKIN_TGA_TYPE_RGBA     2  /**< @brief Uncompressed, raw RGB/A image. */
#define SKIN_TGA_TYPE_GREY     3  /**< @brief Uncompressed, greyscale image. */
#define SKIN_TGA_TYPE_CMAP_RLE 9  /**< @brief Run-length encoded, colourmap. */
#define SKIN_TGA_TYPE_RGBA_RLE 10 /**< @brief Run-length encoded, raw RGB/A. */
#define SKIN_TGA_TYPE_GREY_RLE 11 /**< @brief Run-length encoded, greyscale. */

/**
@brief Write a little-endian UINT16 value to a file.

@param out The file to write to.
@param value The value to write.
*/

static void skinPutUI16_LE(FILE *out, unsigned short value)
{
  fputc((value >> 0) & 0xFF, out);
  fputc((value >> 8) & 0xFF, out);
}

/**
@brief Write a little-endian UINT32 value to a file.

@param out The file to write to.
@param value The value to write.
*/

static void skinPutUI32_LE(FILE *out, unsigned long value)
{
  fputc((value >> 0 ) & 0xFF, out);
  fputc((value >> 8 ) & 0xFF, out);
  fputc((value >> 16) & 0xFF, out);
  fputc((value >> 24) & 0xFF, out);
}

/**
@brief Write the current file position to a given offset within itself.

@param out The file to read the current position from/write that position to.
@param output_position The file offset at which to output the current position.
*/

static void skinWriteCurrentOffset(FILE *out, long output_position)
{
  long stored_position = ftell(out);
  fseek(out, output_position, SEEK_SET);
  skinPutUI32_LE(out, stored_position);
  fseek(out, stored_position, SEEK_SET);
}

/**
@brief Export an image as a Windows (or OS2) BitMaP file.

@todo Support compression, transparency, and larger image sizes.

@param skin The image to be saved.
@param name The filename to be used.
@param flags Any special options, such as `SKIN_FLAG_COMPRESS`, if applicable.
@return `SKIN_STATUS_OKAY` if everything went OK, or a suitable error if not.
*/

static skin_status_t skinSaveBMP(const skin_s *skin, const char *name, skin_flag_t flags)
{
  FILE *out;
  skin_size_t w, h;

  if (skin->w > 65535 || skin->h > 65535) { return SKIN_STATUS_SIZE; }
  out = fopen(name, "wb");
  if (!out) { return SKIN_STATUS_FILE; }

  /* BMP file header: */
  fputc('B', out);
  fputc('M', out);
  skinPutUI32_LE(out, 0); /* File size - filled in later. */
  skinPutUI32_LE(out, 0); /* Reserved. */
  skinPutUI32_LE(out, 0); /* Offset to pixel data - filled in later. */

  /* DIB header: */
  skinPutUI32_LE(out, 12); /* Size of this header. */
  skinPutUI16_LE(out, (unsigned short)skin->w);
  skinPutUI16_LE(out, (unsigned short)skin->h);
  skinPutUI16_LE(out, 1);   /* Number of colour planes. */
  skinPutUI16_LE(out, 24);  /* Bits-per-pixel.          */

  /* Pixel data, vertically-flipped so as to be the right way up: */
  skinWriteCurrentOffset(out, 10);
  for (h = skin->h; h > 0; --h)
  {
    for (w = 0; w < skin->w; ++w)
    {
      long offs = (h - 1) * (skin->w * skin->bpp/8);
      offs += w * skin->bpp/8;

      if (skin->bpp == 24 || skin->bpp == 32)
      {
        fputc(skin->data[offs + 2], out);
        fputc(skin->data[offs + 1], out);
        fputc(skin->data[offs + 0], out);
      }
      else
      {
        fputc(skin->data[offs + 0], out);
        fputc(skin->data[offs + 0], out);
        fputc(skin->data[offs + 0], out);
      }
    }
    /* Each line must be aligned to a 4-byte boundary: */
    for (w *= 3; w % 4; ++w) { fputc(0, out); }
  }
  skinWriteCurrentOffset(out, 2);

  fclose(out);
  return SKIN_STATUS_OKAY;
}

/**
@brief Export an image as a ZSoft PCX file.

@todo Implement this...

@param skin The image to be saved.
@param name The filename to be used.
@param flags Any special options, such as `SKIN_FLAG_COMPRESS`, if applicable.
@return `SKIN_STATUS_OKAY` if everything went OK, or a suitable error if not.
*/

static skin_status_t skinSavePCX(const skin_s *skin, const char *name, skin_flag_t flags)
{
  return SKIN_STATUS_NULL;
}

/**
@brief Export an image as a Portable aNyMap file.

If the "compressed" flag is used, the PGM/PPM output file will be binary rather
than ASCII. PNM files don't have any "real" compression, but this will at least
make the output files smaller.

Note that PNM files can't store alpha data, so any opacity data will be lost.

@param skin The image to be saved.
@param name The filename to be used.
@param flags Any special options, such as `SKIN_FLAG_COMPRESS`, if applicable.
@return `SKIN_STATUS_OKAY` if everything went OK, or a suitable error if not.
*/

static skin_status_t skinSavePNM(const skin_s *skin, const char *name, skin_flag_t flags)
{
  FILE *out;
  unsigned long i;

  out = fopen(name, "wb");
  if (!out) { return SKIN_STATUS_FILE; }

  fprintf(out, "P");
  if (skin->bpp == 8 || skin->bpp == 16)
  {
    fprintf(out, "%d", flags & SKIN_FLAG_COMPRESS ?
        SKIN_PNM_TYPE_GREY_BIN : SKIN_PNM_TYPE_GREY);
  }
  else
  {
    fprintf(out, "%d", flags & SKIN_FLAG_COMPRESS ?
        SKIN_PNM_TYPE_RGBA_BIN : SKIN_PNM_TYPE_RGBA);
  }
  fprintf(out, " %lu %lu 255\n", skin->w, skin->h);

  for (i = 0; i < skin->w * skin->h; ++i)
  {
    const unsigned long offs = i * skin->bpp/8;
    if (skin->bpp == 24 || skin->bpp == 32)
    {
      if (flags & SKIN_FLAG_COMPRESS)
      {
        fputc(skin->data[offs + 0], out);
        fputc(skin->data[offs + 1], out);
        fputc(skin->data[offs + 2], out);
      }
      else
      {
        fprintf(out, "%d %d %d\n",
            skin->data[offs + 0], skin->data[offs + 1], skin->data[offs + 2]);
      }
    }
    else
    {
      if (flags & SKIN_FLAG_COMPRESS) { fputc(skin->data[offs], out);           }
      else                            { fprintf(out, "%d\n", skin->data[offs]); }
    }
  }

  fclose(out);
  return SKIN_STATUS_OKAY;
}

/**
@brief Export an image as a Truevision TarGA file.

@todo Respect the "compressed" flag!

@param skin The image to be saved.
@param name The filename to be used.
@param flags Any special options, such as `SKIN_FLAG_COMPRESS`, if applicable.
@return `SKIN_STATUS_OKAY` if everything went OK, or a suitable error if not.
*/

static skin_status_t skinSaveTGA(const skin_s *skin, const char *name, skin_flag_t flags)
{
  FILE *out;
  unsigned long i;

  out = fopen(name, "wb");
  if (!out) { return SKIN_STATUS_FILE; }

  fputc(0, out); /* N/A: ID string size. */
  fputc(0, out); /* N/A: Colourmap type. */

  if (skin->bpp == 8 || skin->bpp == 16)
  {
    fputc(SKIN_TGA_TYPE_GREY, out);
  }
  else
  {
    fputc(SKIN_TGA_TYPE_RGBA, out);
  }

  skinPutUI16_LE(out, 0); /* N/A: Colourmap origin. */
  skinPutUI16_LE(out, 0); /* N/A: Colourmap length. */
  fputc(0, out);  /* N/A: Colourmap bits-per-pixel. */

  skinPutUI16_LE(out, 0); /* Image X origin. */
  skinPutUI16_LE(out, 0); /* Image Y origin. */
  skinPutUI16_LE(out, skin->w);
  skinPutUI16_LE(out, skin->h);
  fputc(skin->bpp, out);
  fputc(32, out);  /* Flags: Invert image. */

  for (i = 0; i < skin->w * skin->h; ++i)
  {
    const unsigned long offs = i * skin->bpp/8;
    if (skin->bpp == 24 || skin->bpp == 32)
    {
      fputc(skin->data[offs + 2], out);
      fputc(skin->data[offs + 1], out);
      fputc(skin->data[offs + 0], out);
      if (skin->bpp == 32) { fputc(skin->data[offs + 3], out); }
    }
    else
    {
      fputc(skin->data[offs + 0], out);
      if (skin->bpp == 16) { fputc(skin->data[offs + 1], out); }
    }
  }

  fclose(out);
  return SKIN_STATUS_OKAY;
}

/*
[PUBLIC] Save a single image to a file.
*/

skin_status_t skinSaveFile(const skin_s *skin, const char *name, skin_format_t format, skin_flag_t flags)
{
  if (!skin || !name || !name[0]) { return SKIN_STATUS_NULL; }
  switch (format)
  {
    case SKIN_FORMAT_BMP: return skinSaveBMP(skin, name, flags);
    case SKIN_FORMAT_PCX: return skinSavePCX(skin, name, flags);
    case SKIN_FORMAT_PNM: return skinSavePNM(skin, name, flags);
    case SKIN_FORMAT_TGA: return skinSaveTGA(skin, name, flags);
    default: break;
  }
  return SKIN_STATUS_NULL;
}
