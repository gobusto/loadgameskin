/**
@file
@brief Any image-related functions (other than loading/saving) live here.

Copyright (C) 2016 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "skin.h"

/*
[PUBLIC] Create a new (empty) group of images.
*/

skin_set_s *skinSetCreate(skin_count_t num_items)
{
  skin_set_s *skin_set;

  if (num_items < 1) { return NULL; }

  skin_set = (skin_set_s*)malloc(sizeof(skin_set_s));
  if (!skin_set) { return NULL; }
  memset(skin_set, 0, sizeof(skin_set_s));

  skin_set->item = (skin_s**)malloc(sizeof(skin_s*) * num_items);
  if (!skin_set->item) { free(skin_set); return NULL; }
  memset(skin_set->item, 0, sizeof(skin_s*) * num_items);

  skin_set->num_items = num_items;

  return skin_set;
}

/*
[PUBLIC] Destroy a previously-created image group.
*/

void skinSetDelete(skin_set_s *skin_set)
{
  if (!skin_set) { return; }
  if (skin_set->item)
  {
    while (skin_set->num_items) { skinDelete(skin_set->item[--skin_set->num_items]); }
    free(skin_set->item);
  }
  free(skin_set);
}

/*
[PUBLIC] Create a new (blank) image structure.
*/

skin_s *skinCreate(skin_size_t w, skin_size_t h, skin_bits_t bpp, const char *name)
{
  skin_s *skin;

  if (w < 1 || h < 1) { return NULL; }
  else if (bpp != 8 && bpp != 16 && bpp != 24 && bpp != 32) { return NULL; }

  skin = (skin_s*)malloc(sizeof(skin_s));
  if (!skin) { return NULL; }
  memset(skin, 0, sizeof(skin_s));

  skin->data = (skin_data_t*)malloc(sizeof(skin_data_t) * w * h * bpp/8);
  if (!skin->data) { free(skin); return NULL; }

  skin->w = w;
  skin->h = h;
  skin->bpp = bpp;
  if (name) { strncpy(skin->name, name, SKIN_MAXNAME-1); }

  return skin;
}

/*
[PUBLIC] Destroy a previously-created image.
*/

void skinDelete(skin_s *skin)
{
  if (!skin) { return; }
  free(skin->data);
  free(skin);
}
