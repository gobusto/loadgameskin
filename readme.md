Game Image Import/Conversion Utilities
======================================

This code imports sprites, textures, or other images from special game-specific
formats. They can then be exported as a more-common file-type (such as TGA) for
viewing or editing.

In-memory images are stored in one of the following formats:

+ 8-bit Greyscale
+ 16-bit Greyscale + Alpha
+ 24-bit RGB
+ 32-bit RGB + Alpha

The pixel data is stored without any per-line padding (unlike, for example, BMP
files). Images with palettes are converted into "raw" pixels when loaded, so no
actual palette data is stored in-memory.

Supported Formats
-----------------

The current version of the code allows the following formats to be loaded:

+ Battle Construction Vehicles AGI
+ Robot Warlords ETI
+ Chasm: The Rift OBJ
+ Red Faction PEG

Once loaded, images can be exported in the following formats:

+ Windows (or OS/2) BMP
+ Portable aNyMap PNM
+ Truevision TGA

A simple program called `xxx2tga` is provided to illustrate how the code may be
used, though all files are fully DOXYGEN-ified and should be self-explanatory.

TODO
----

Various additional import formats will be added in the future, including:

+ Chasm: The Rift 3O/CAR
+ Quake 1 MDL, BSP, SPR, and LMP
+ Quake 2 SP2
+ Choro-Q APT
+ Half Life (PS2) PSI
+ Playstation TIM

Note that some of these are already "convertable" using specific programs found
within the `docs` subdirectory, but the main codebase doesn't support them yet.

The file export code also needs improving:

+ ZSoft PCX export is currently stubbed, but unimplemented.
+ Compressed BMP/TGA files aren't supported yet
+ BMP files can't store images with a width/height larger than 65535 pixels.
+ BMP files don't store alpha data.

Additional export formats might be added in the future if they're simple enough
to implement without requiring additional libraries.

Licensing
---------

This code is made available under the [MIT](http://opensource.org/licenses/MIT)
license, which allows you to use or modify it for any purpose including paid-for
and closed-source projects. All I ask in return is that proper attribution is
given (i.e. don't remove my name from the copyright text in the source code, and
perhaps include me on the "credits" screen, if your program has such a thing).
