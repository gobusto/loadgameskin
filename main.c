/**
@file
@brief A simple xxx-to-TGA conversion program.

This is not part of the actual image loading code; it's for testing purposes.

Copyright (C) 2016 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include "skin/skin.h"

/**
@brief The program entry point.

@param argc Length of the argv array.
@param argv Array of progam arguments.
@return Zero on success, or non-zero on failure.
*/

int main(int argc, char **argv)
{
  skin_set_s *skinset = NULL;
  skin_s     *palette = NULL;
  skin_count_t i;

  /* First, attempt to load the palette file (if one has been specified). */
  if (argc == 3)
  {
    palette = skinPaletteLoadFile(argv[2]);
    if (!palette)
    {
      printf("ERROR: Could not load palette file: %s\n", argv[2]);
      return 1;
    }
  }
  else if (argc != 2)
  {
    printf("Usage: %s input.img [input.pal]\n", argv[0]);
    return 1;
  }

  /* Next, attempt to load the actual image file and extract the images. */
  skinset = skinSetLoadFile(argv[1], palette);
  if (!skinset)
  {
    printf("ERROR: Could not load image file: %s\n", argv[1]);
  }
  else for (i = 0; i < skinset->num_items; ++i)
  {
    const skin_s *skin = skinset->item[i];
    const char *name;

    char placeholder[512];

    if (skin->name && skin->name[0])
    {
      /*
      If the skin has an explicit name, use it. Useful for cases where the name
      is important, and you don't want to have to rename everything afterwards.

      Beware: This name might be something like `/path/to/important.file` - if
      you want to be safe, consider adding `&& 0` to the `if` condition above.

      Also, note that this won't work if the name specifies a subdirectory that
      doesn't exist: `path/to/MISSING/folder.ext`
      */

      name = skin->name;
    }
    else if (skinset->num_items == 1 && 0)
    {
      /*
      The skin doesn't have a name, but there's only one image, so give the one
      output file the same name as the input file, with a filetype glued on.

      Note that the output file will appear in the same folder as the "orignal"
      image, as it will inherit the `path/to/file.ext` prefix as well. This can
      be disabled in the same way as the "use original name" logic above.

      Also, note that this will break if the input file name is very long - the
      `placeholder` buffer might not be long enough to store it!
      */

      sprintf(placeholder, "%s.tga", argv[1]);
      name = placeholder;
    }
    else
    {
      /* Use a boring-but-safe name for this output file. */
      sprintf(placeholder, "image%lu.tga", i);
      name = placeholder;
    }

    if (skinSaveFile(skin, name, SKIN_FORMAT_TGA, 0) == SKIN_STATUS_OKAY)
    { printf("[OKAY] %s\n", name); }
    else
    { printf("[FAIL] %s\n", name); }
  }

  /* Finally, free the palette/image set and end the program. */
  skinDelete(palette);
  skinSetDelete(skinset);
  return 0;
}
