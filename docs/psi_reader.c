/**
@file
@brief A simple program for converting PS2 Half-Life PSI files into TGA format

Copyright (C) 2016 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define PSI_HEAD_SIZE 32
#define PSI_NAME_SIZE 20

#define PSI_VER_CMAP 2
#define PSI_VER_RGBA 5

static unsigned long read_ui16(FILE *src)
{
  unsigned long result;
  if (!src) { return 0; }
  result =  (unsigned long)fgetc(src) << 0;
  result += (unsigned long)fgetc(src) << 8;
  return result;
}

static unsigned long read_ui32(FILE *src)
{
  unsigned long result;
  if (!src) { return 0; }
  result =  (unsigned long)fgetc(src) << 0;
  result += (unsigned long)fgetc(src) << 8;
  result += (unsigned long)fgetc(src) << 16;
  result += (unsigned long)fgetc(src) << 24;
  return result;
}

static int psiSaveImage(const char *name, unsigned long version, unsigned short w, unsigned short h, FILE *src)
{
  FILE *out;
  unsigned long i;
  unsigned char palette[256*4];

  if (version != PSI_VER_CMAP && version != PSI_VER_RGBA) { return 0; }
  out = fopen(name, "wb");
  if (!out) { return 0; }

  fputc(0, out);
  fputc(0, out);
  fputc(2, out); /* Uncompressed RGB(A). */
  fputc(0, out); fputc(0, out);
  fputc(0, out); fputc(0, out);
  fputc(0, out);
  fputc(0, out); fputc(0, out); /* X origin. */
  fputc(0, out); fputc(0, out); /* Y origin. */
  fputc((w >> 0) & 0xFF, out); fputc((w >> 8) & 0xFF, out);
  fputc((h >> 0) & 0xFF, out); fputc((h >> 8) & 0xFF, out);
  fputc(32, out); /* Bits-per-pixel. */
  fputc(1 << 5, out);  /* Inverted origin. */

  if (version == PSI_VER_CMAP) { fread(palette, 1, 256*4, src); }

  for (i = 0; i < w*h; ++i)
  {
    int rgba[4];

    if (version == PSI_VER_RGBA)
    {
      rgba[0] = fgetc(src);
      rgba[1] = fgetc(src);
      rgba[2] = fgetc(src);
      rgba[3] = fgetc(src);
    }
    else
    {
      /* FIXME: This doesn't seem to be quite right... */
      unsigned char pal_index = fgetc(src);
      rgba[0] = palette[pal_index*4 + 0];
      rgba[1] = palette[pal_index*4 + 1];
      rgba[2] = palette[pal_index*4 + 2];
      rgba[3] = palette[pal_index*4 + 3];
    }

    /* Values are usually in the range 0-127 (or sometimes 128) so fix them: */
    {
      int e;
      for(e = 0; e < 4; ++e)
      {
        rgba[e] *= 2;
        if (rgba[e] > 256) { fprintf(stderr, "WARNING: rgba[%d] is %d\n", e, rgba[e]); }
        if (rgba[e] > 255) { rgba[e] = 255; }
      }
    }

    fputc(rgba[2], out);
    fputc(rgba[1], out);
    fputc(rgba[0], out);
    fputc(rgba[3], out);
  }

  fclose(out);
  return 1;
}

int main(int argc, char *argv[])
{
  FILE *src;
  long length;

  char name[PSI_NAME_SIZE + 1];
  unsigned long version;
  unsigned short w, h, w2, h2;

  if (argc != 3)
  {
    fprintf(stderr, "Usage: psi_reader input.psi output.tga\n");
    return 0;
  }

  src = fopen(argv[1], "rb");
  if (!src)
  {
    fprintf(stderr, "Could not read file.\n");
    return 1;
  }

  fseek(src, 0, SEEK_END);
  length = ftell(src);
  if (length < PSI_HEAD_SIZE + 4)
  {
    fprintf(stderr, "File is too short.\n");
    fclose(src);
    return 2;
  }

  rewind(src);
  memset(name, 0, PSI_NAME_SIZE + 1);
  fread(name, 1, PSI_NAME_SIZE, src);
  version = read_ui32(src);
  w = read_ui16(src);
  h = read_ui16(src);
  w2 = read_ui16(src);
  h2 = read_ui16(src);

  fprintf(stderr, "%s (v%lu) - %ld bytes\n", name, version, length);
  fprintf(stderr, "\t%dx%d (%dx%d)\n", w, h, w2, h2);

  if (w < 1 || h < 1 || w != w2 || h != h2)
  { fprintf(stderr, "Unexpected value(s) - image not converted.\n"); }
  else if (version == PSI_VER_CMAP && PSI_HEAD_SIZE + 256*4 + w*h > length)
  { fprintf(stderr, "File is too short - image not converted.\n"); }
  else if (version == PSI_VER_RGBA && PSI_HEAD_SIZE + w*h*4 > length)
  { fprintf(stderr, "File is too short - image not converted.\n"); }
  else if (!psiSaveImage(argv[2], version, w, h, src))
  { fprintf(stderr, "Could not create output file.\n"); }

  fclose(src);
  return 0;
}
