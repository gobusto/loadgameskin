/**
@file
@brief A simple program for converting Chasm: The Rift OBJ files into TGA format

Note that Chasm OBJ files are actually animated sprites; they have nothing to do
with the 3D model format commonly used by various 3D mesh editors.

Copyright (C) 2016 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

signed short read_si16(FILE *src)
{
  signed short i = (signed short)fgetc(src);
  i += (signed char)fgetc(src) << 8;
  return i;
}

int palLoadFile(const char *file_name, unsigned char *buffer)
{
  FILE *pal = fopen(file_name, "rb");
  if (!pal) { return -1; }
  fseek(pal, 0, SEEK_END);

  if (ftell(pal) != 768)
  {
    fclose(pal);
    return 1;
  }
  else if (buffer)
  {
    rewind(pal);
    fread(buffer, 1, 768, pal);
  }

  fclose(pal);
  return 0;
}

int main(int argc, char *argv[])
{
  unsigned char palette[256 * 3];

  FILE *src;
  short num_images, i;

  if (palLoadFile("CHASM2.PAL", palette) != 0)
  {
    fprintf(stderr, "Could not load palette file.\n");
    return 1;
  }

  src = fopen(argv[1], "rb");
  if (!src) { return 1; }
  num_images = read_si16(src);

  for (i = 0; i < num_images; ++i)
  {
    char output_file_name[64];
    short img_w, img_h;
    long a;
    FILE *fptr;

    img_h = read_si16(src);
    img_w = read_si16(src);
    read_si16(src); /* Unknown... */

    sprintf(output_file_name, "image%d.tga", i);

    fprintf(stderr, "Saving %s (%d x %d)\n", output_file_name, img_w, img_h);

    fptr = fopen(output_file_name, "wb");
    if (fptr)
    {
      /* Save the image as a TGA: http://paulbourke.net/dataformats/tga/ */
      putc(0,fptr);
      putc(0,fptr);
      putc(2,fptr);
      putc(0,fptr); putc(0,fptr);
      putc(0,fptr); putc(0,fptr);
      putc(0,fptr);
      /* X and Y origin : */
      putc(0,fptr); putc(0,fptr);
      putc(0,fptr); putc(0,fptr);
      /* Image width, height, and bit depth: */
      putc((img_w & 0x00FF),fptr);
      putc((img_w & 0xFF00) / 256,fptr);
      putc((img_h & 0x00FF),fptr);
      putc((img_h & 0xFF00) / 256,fptr);
      putc(32,fptr);
      putc(0,fptr);
    }

    /* The RGB values in the palette range from 0-63, so multiply them by 4. */
    for (a = 0; a < img_w * img_h; ++a)
    {
      unsigned char palette_index = fgetc(src);
      if (fptr)
      {
        /* The palette stores RGBRGBRGB values but TGA files store BGRABGRA. */
        fputc(palette[palette_index*3 + 2] * 4, fptr);
        fputc(palette[palette_index*3 + 1] * 4, fptr);
        fputc(palette[palette_index*3 + 0] * 4, fptr);
        /* 255 is a special value used to indicate fully transparent pixels. */
        fputc(palette_index == 255 ? 0 : 255, fptr);
      }
    }

    fclose(fptr);
  }

  fclose(src);
  return 0;
}
