/**
@file
@brief A simple program for converting Red Faction PEG files into TGA format

Copyright (C) 2016 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define PEG_HEAD_SIZE 32  /**< @brief Size of the PEG header in bytes.       */
#define PEG_INFO_SIZE 64  /**< @brief Size of each PEG "info" item in bytes. */

#define PEG_4CC 0x564b4547  /**< @brief Spells "GEKV" if read as ASCII text. */
#define PEG_VER 6           /**< @brief PEG file format version.             */

static unsigned long read_ui16(FILE *src)
{
  unsigned long result;
  if (!src) { return 0; }
  result =  (unsigned long)fgetc(src) << 0;
  result += (unsigned long)fgetc(src) << 8;
  return result;
}

static unsigned long read_ui32(FILE *src)
{
  unsigned long result;
  if (!src) { return 0; }
  result =  (unsigned long)fgetc(src) << 0;
  result += (unsigned long)fgetc(src) << 8;
  result += (unsigned long)fgetc(src) << 16;
  result += (unsigned long)fgetc(src) << 24;
  return result;
}

static void pegSaveImage(const char *name, unsigned short w, unsigned short h, unsigned char word_count, FILE *src, unsigned long offset)
{
  unsigned char palette[256*4];
  FILE *out;
  unsigned long i;

  fseek(src, offset, SEEK_SET);
  for (i = 0; i < 256; ++i)
  {
    if (word_count == 1)
    {
      unsigned short value = read_ui16(src);
      palette[i*4+0] = ((value >> 0 ) & 31) * 8;
      palette[i*4+1] = ((value >> 5 ) & 31) * 8;
      palette[i*4+2] = ((value >> 10) & 31) * 8;
      palette[i*4+3] = ((value >> 15) & 1 ) * 255;
    }
    else if (word_count == 2)
    {
      palette[i*4+0] = fgetc(src);
      palette[i*4+1] = fgetc(src);
      palette[i*4+2] = fgetc(src);
      palette[i*4+3] = (fgetc(src) > 0) * 255;  /* Not sure how this works... */
    }
    else if (word_count != 0)
    {
      fprintf(stderr, "Unexpected word count: %d\n", word_count);
      return;
    }
  }

  out = fopen(name, "wb");
  if (!out) { return; }

  fputc(0, out);
  fputc(0, out);
  fputc(2, out); /* Uncompressed RGB(A). */
  fputc(0, out); fputc(0, out);
  fputc(0, out); fputc(0, out);
  fputc(0, out);
  fputc(0, out); fputc(0, out); /* X origin. */
  fputc(0, out); fputc(0, out); /* Y origin. */
  fputc((w >> 0) & 0xFF, out); fputc((w >> 8) & 0xFF, out);
  fputc((h >> 0) & 0xFF, out); fputc((h >> 8) & 0xFF, out);
  fputc(32, out); /* Bits-per-pixel. */
  fputc(1 << 5, out);  /* Inverted origin. */

  for (i = 0; i < w*h; ++i)
  {
    if (word_count == 0)
    {
      int r, g, b, a;
      r = fgetc(src);
      g = fgetc(src);
      b = fgetc(src);
      a = fgetc(src);
      fputc(b, out);
      fputc(g, out);
      fputc(r, out);
      fputc(a, out);
    }
    else
    {
      int pal_index = fgetc(src);
      fputc(palette[pal_index*4+2], out);
      fputc(palette[pal_index*4+1], out);
      fputc(palette[pal_index*4+0], out);
      fputc(palette[pal_index*4+3], out);
    }
  }

  fclose(out);
}

int main(int argc, char *argv[])
{
  FILE *src;
  long len;
  unsigned long version, info_size, data_size, num_info, num_data, word_size, i;

  src = fopen(argv[1], "rb");
  if (!src)
  {
    fprintf(stderr, "Could not open file.\n");
    return 1;
  }

  fseek(src, 0, SEEK_END);
  len = ftell(src);
  fprintf(stderr, "File Name: %s\nFile Size: %lu\n", argv[1], len);

  if (len < PEG_HEAD_SIZE + PEG_INFO_SIZE)
  {
    fprintf(stderr, "File is too short.\n");
    goto end_program;
  }

  rewind(src);
  if (read_ui32(src) != PEG_4CC)
  {
    fprintf(stderr, "This does not appear to be a valid PEG file.\n");
    goto end_program;
  }

  version   = read_ui32(src);
  info_size = read_ui32(src);
  data_size = read_ui32(src);
  num_info  = read_ui32(src);
  i         = read_ui32(src);
  num_data  = read_ui32(src);
  word_size = read_ui32(src);

  fprintf(stderr, "Version: %lu\n", version);   /* Always 6.      */
  fprintf(stderr, "LenInfo: %lu\n", info_size);
  fprintf(stderr, "LenData: %lu\n", data_size);
  fprintf(stderr, "Images1: %lu\n", num_info);
  fprintf(stderr, "UNKNOWN: %lu\n", i);         /* Always 0.      */
  fprintf(stderr, "Images2: %lu\n", num_data);  /* Same as above. */
  fprintf(stderr, "WordBit: %lu\n", word_size); /* Always 16.     */

  if (version != PEG_VER || word_size != 16 || i ||
      num_info < 1 || num_data < 1 || num_info > num_data ||
      info_size % PEG_INFO_SIZE || info_size != num_info * PEG_INFO_SIZE ||
      len < PEG_HEAD_SIZE + info_size + data_size)
  {
    fprintf(stderr, "Something about this header seems inconsistent...\n");
    goto end_program;
  }

  for (i = 0; i < num_info; ++i)
  {
    char name[49];
    unsigned short w, h, unknown_a, unknown_b, unknown_c;
    unsigned char flags, palette_size;
    unsigned long offset;

    fseek(src, PEG_HEAD_SIZE + PEG_INFO_SIZE * i, SEEK_SET);

    w = read_ui16(src);
    h = read_ui16(src);
    flags = fgetc(src);         /* 7 for raw RGBA images, or 4 for palettes. */
    palette_size = fgetc(src);  /* 0 = RGBA, 1/2 = 16/32-bit palette. */
    unknown_a = read_ui16(src); /* Flags? */
    unknown_b = read_ui16(src); /* Flags? */
    unknown_c = read_ui16(src); /* ???? (Data size is bigger if this is >0). */
    memset(name, 0, 49); fread(name, 1, 48, src);
    offset = read_ui32(src);

    fprintf(stderr, "\nImage %lu: %s (%dx%d)\n", i, name, w, h);
    fprintf(stderr, "\tFlagBits: %d\n", flags);
    fprintf(stderr, "\tDataType: %d\n", palette_size);
    fprintf(stderr, "\tUnknownA: %d\n", unknown_a);
    fprintf(stderr, "\tUnknownB: %d\n", unknown_b);
    fprintf(stderr, "\tUnknownC: %d\n", unknown_c);
    fprintf(stderr, "\tDataOffs: %lu\n", offset);

    pegSaveImage(name, w, h, palette_size, src, offset);

    /* TODO: unknown_c affects the size somehow if it's not zero... */
    #if 0
    {
      const unsigned long length = w*h + palette_size*512;
      fprintf(stderr, "\t\tThe calculated length is: %lu\n", length);
      fprintf(stderr, "\t\tThe next offset would be: %lu\n", offset + length);
    }
    #endif
  }

  end_program:
  fclose(src);
  return 0;
}
