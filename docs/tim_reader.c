/**
@file
@brief A simple program for converting Playstation TIM files into PPM format

Copyright (C) 2016 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>

/* Constants: */

#define TIM_4CC 16

#define TIM_HEAD_SIZE 8
#define TIM_SUBHEAD_SIZE 12

#define TIM_BITS_4BPP  0
#define TIM_BITS_8BPP  1
#define TIM_BITS_16BPP 2
#define TIM_BITS_24BPP 3
#define TIM_BITS_MASK  0x3
#define TIM_FLAG_PAL   0x8

unsigned short read_ui16(FILE *src)
{
  unsigned short i = (unsigned short)fgetc(src);
  i += (unsigned short)fgetc(src) << 8;
  return i;
}

unsigned long read_ui32(FILE *src)
{
  unsigned long i = (unsigned long)fgetc(src);
  i += (unsigned long)fgetc(src) << 8;
  i += (unsigned long)fgetc(src) << 16;
  i += (unsigned long)fgetc(src) << 24;
  return i;
}

/**
@brief Unpack a 16-bit RGB value into three separate bytes.

Each channel is stored as a 5 byte valuem so technically it's 15-bit, not 16...

@param src The file to read from.
@param rgb The byte buffer to write the result to.
*/

static void read_rgb(FILE *src, unsigned char *rgb)
{
  unsigned short value = read_ui16(src);
  rgb[0] = ((value >> 0 ) & 31) * 8;
  rgb[1] = ((value >> 5 ) & 31) * 8;
  rgb[2] = ((value >> 10) & 31) * 8;
}

/**
@brief Read an RGB value from the palette.

This is simply a convenient wrapper around the `read_rgb()` function.

@param src The file to read from.
@param rgb The byte buffer to write the result to.
@param pal_id The palette index to read.
@param palette_used The index of the palette to use.
*/

static void read_pal_rgb(FILE *src, unsigned char *rgb, unsigned char pal_id, unsigned short palette_used)
{
  long old_file_offset = ftell(src);
  unsigned short num_colours, num_palettes;

  /* Determine how many palettes are available, and how long each one is: */
  fseek(src, TIM_HEAD_SIZE + 8, SEEK_SET);
  num_colours = read_ui16(src);   /* i.e. palette image "width" value. */
  num_palettes = read_ui16(src);  /* i.e. palette image "height" value. */

  if (palette_used < num_palettes)
  {
    fseek(src, (palette_used * num_colours*2) + (pal_id*2), SEEK_CUR);
    read_rgb(src, rgb);
  }
  else
  {
    fprintf(stderr, "WARNING: Invalid palette used!\n");
    rgb[0] = 0; rgb[1] = 0; rgb[2] = 0;
  }

  /* Go back to where we were before: */
  fseek(src, old_file_offset, SEEK_SET);
}

int read_file(FILE *src)
{
  long length, image_offs, eof_offs, i;
  unsigned long flags;
  unsigned short w, h, num_pals;

  fseek(src, 0, SEEK_END);
  length = ftell(src);
  rewind(src);

  /* NOTE: Some TIM files don't seem to have a file header; they fail here: */
  if (length < TIM_HEAD_SIZE) { return fprintf(stderr, "File is too short\n"); }
  if (read_ui32(src) != TIM_4CC) { return fprintf(stderr, "Invalid 4CC\n"); }

  /* Only a few combinations of bit-depth and palette-ness are commonly used: */
  flags = read_ui32(src);
  if (flags != TIM_BITS_4BPP + TIM_FLAG_PAL && flags != TIM_BITS_16BPP &&
      flags != TIM_BITS_8BPP + TIM_FLAG_PAL && flags != TIM_BITS_24BPP)
  { return fprintf(stderr, "Unknown TIM flags\n"); }

  /* If a palette is present, it appears before the actual image data does: */
  if (flags & TIM_FLAG_PAL)
  {
    /* The palette data uses the same format as an actual image: */
    image_offs = read_ui32(src);
    read_ui32(src); /* Always zero? Maybe origin X/Y as 2x UI16? */
    w = read_ui16(src);
    h = read_ui16(src);

    /* Ensure that the reported values match our expectations: */
    switch (flags & TIM_BITS_MASK)
    {
      case TIM_BITS_4BPP:
        if (image_offs != 12 + w*h*2) { return fprintf(stderr, "Unexpected offset\n"); }
        /* Various files from the Resident Evil 2 demo have this as 32...? */
        if (w != 16) { fprintf(stderr, "Expected 16 colours, got %d\n", w); }
      break;

      case TIM_BITS_8BPP:
        if (image_offs != 12 + w*h*2) { return fprintf(stderr, "Unexpected offset\n"); }
        if (w != 256) { fprintf(stderr, "Expected 256 colours, got %d\n", w); }
      break;

      default: break;
    }
    if (h < 1) { return fprintf(stderr, "Expected at least one palette\n"); }

    /* Fix the image offset so that it accounts for the file header as well: */
    image_offs += TIM_HEAD_SIZE;
    num_pals = h;
  }
  else
  {
    image_offs = TIM_HEAD_SIZE;
    num_pals = 0;
  }

  /* Read the actual image data. */
  fseek(src, image_offs, SEEK_SET);
  eof_offs = image_offs + (long)read_ui32(src);
  if (eof_offs > length) { return fprintf(stderr, "Unexpected EOF offset\n"); }
  read_ui32(src); /* Always zero? Maybe origin X/Y as 2x UI16? */
  w = read_ui16(src);
  h = read_ui16(src);

  /*
  The given "width" is actually more like "number of 16-bit values per row"; as
  such, the bit depth of the image affects how the "true" width is interpreted:
  */

  switch (flags & TIM_BITS_MASK)
  {
    case TIM_BITS_4BPP:  w *= 4;        fprintf(stderr, "4");  break;
    case TIM_BITS_8BPP:  w *= 2;        fprintf(stderr, "8");  break;
    case TIM_BITS_16BPP: w *= 1;        fprintf(stderr, "16"); break;
    case TIM_BITS_24BPP: w = (w*2) / 3; fprintf(stderr, "24"); break;
    default: break;
  }
  fprintf(stderr, "bpp - %dx%d", w, h);
  if (flags & TIM_FLAG_PAL) { fprintf(stderr, " (%d palettes)", num_pals); }
  fprintf(stderr, "\n");

  /*
  Current status:

  + 4BPP with palette = tested, works.
  + 8BPP with palette = tested, works.
  + 16BPP, no palette = tested, works.
  + 24BPP, no palette = not tested...

  I also haven't managed to find any images with non-even widths, so I'm not sure
  if I need to account for "padding" bytes at the end of each row...
  */

  fprintf(stdout, "P3 %d %d 255\n", w, h);
  for (i = 0; i < (long)w*h; ++i)
  {
    unsigned char rgb[3];
    unsigned short palette_used;

    /*
    I'm not sure if there's any way to detect which palette to use for each bit
    of the image, so for now we'll just always use the first one available:
    */

    palette_used = 0;

    /* Read the actual pixel data: */
    switch (flags & TIM_BITS_MASK)
    {
      case TIM_BITS_4BPP:
      {
        unsigned char pal_id = fgetc(src);
        if (i % 2) { pal_id = (pal_id >> 4); }
        else { fseek(src, -1, SEEK_CUR); }
        read_pal_rgb(src, rgb, pal_id & 0x0F, palette_used);
      }
      break;

      case TIM_BITS_8BPP:
      {
        unsigned char pal_id = fgetc(src);
        /* The PS1 doesn't seem to require index swapping like PS2 games do. */
        read_pal_rgb(src, rgb, pal_id, palette_used);
      }
      break;

      case TIM_BITS_16BPP:
        read_rgb(src, rgb);
      break;

      case TIM_BITS_24BPP:
        rgb[0] = fgetc(src);
        rgb[1] = fgetc(src);
        rgb[2] = fgetc(src);
      break;

      default: break;
    }

    fprintf(stdout, "%d %d %d\n", rgb[0], rgb[1], rgb[2]);
  }

  return 0;
}

int main(int argc, char **argv)
{
  int i;
  for (i = 1; i < argc; ++i)
  {
    FILE *src = fopen(argv[i], "rb");
    if (src) { read_file(src); fclose(src); }
    else     { fprintf(stderr, "Could not open %s\n", argv[i]);  }
  }
  return 0;
}
