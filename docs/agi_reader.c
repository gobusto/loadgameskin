/**
@file
@brief A simple program for converting BCV .AGI files into .PPM format

Copyright (C) 2016 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

#define AGI_4CC 0x00000020
#define AGI_HEAD_SIZE 48

static unsigned short read_ui16(FILE *src)
{
  unsigned short i = (unsigned short)fgetc(src);
  i += (unsigned short)fgetc(src) << 8;
  return i;
}

static unsigned long read_ui32(FILE *src)
{
  unsigned long i = (unsigned long)fgetc(src);
  i += (unsigned long)fgetc(src) << 8;
  i += (unsigned long)fgetc(src) << 16;
  i += (unsigned long)fgetc(src) << 24;
  return i;
}

static int read_agi(FILE *src)
{
  long length, pal_offs, i;
  unsigned short w, h, pal_size;

  if (!src) { return 0x5ADFACE; }

  fseek(src, 0, SEEK_END);
  length = ftell(src);
  rewind(src);

  if (length < AGI_HEAD_SIZE + 1 + 4) { return fprintf(stderr, "File is too short\n"); }

  if (read_ui32(src) != AGI_4CC) { return fprintf(stderr, "Invalid 4CC\n"); }
  /* I'm guessing that this is the number of images... */
  if (read_ui16(src) != 1) { return fprintf(stderr, "Expected only one item\n"); }
  /* ...and this is the number of palettes... */
  if (read_ui16(src) != 1) { return fprintf(stderr, "Expected only one item\n"); }

  /* The image should come after the header, so this offset shouldn't vary: */
  if (read_ui32(src) != AGI_HEAD_SIZE) { return fprintf(stderr, "Unexpected image offset\n"); }
  read_ui32(src); /* (Unknown) */
  read_ui32(src); /* (Unknown) */
  read_ui32(src); /* (Unknown) */
  w = read_ui16(src);
  h = read_ui16(src);

  /* Next comes the palette data: */
  pal_offs = (long)read_ui32(src);
  read_ui32(src); /* (Unknown) */
  read_ui32(src); /* Always 1? */
  read_ui32(src); /* (Unknown) */
  pal_size = read_ui16(src);
  pal_size *= read_ui16(src);

  /* Only 16-colour and 256-colour .AGI files are supported: */
  if (pal_size != 16 && pal_size != 256)
  {
    return fprintf(stderr, "Unexpected palette size\n");
  }
  else if (0)
  {
    /* Ensure that the various offsets make sense: */
    long data_size = w*h / (pal_size == 16 ? 2 : 1);  /* TODO: Round up! */
    if (AGI_HEAD_SIZE + data_size > pal_offs) { return fprintf(stderr, "Image data is too long\n"); }
    if (pal_offs + (pal_size * 4) > length) { return fprintf(stderr, "Palette data is too long\n"); }
    if (pal_offs != AGI_HEAD_SIZE + data_size) { return fprintf(stderr, "Unexpected palette offset\n"); }
  }
#if 1
  fprintf(stderr, "Image is %dx%d with %d palette entries\n", w, h, pal_size);
#endif
  fprintf(stdout, "P3 %d %d 255\n", w, h);

  for (i = 0; i < w*h; ++i)
  {
    unsigned char rgba[4];
    unsigned char pal_id;
    long old_position;

    pal_id = fgetc(src);

    if (pal_size == 16)
    {
      if (i % 2) { pal_id = (pal_id >> 4); }
      else { fseek(src, -1, SEEK_CUR); }
      pal_id = pal_id & 0x0F;
    }
    else
    {
      char temp = pal_id % 32;
      if (temp >= 8  && temp < 16) { pal_id += 8; }
      if (temp >= 16 && temp < 24) { pal_id -= 8; }
    }

    old_position = ftell(src);
    fseek(src, pal_offs + pal_id*4, SEEK_SET);
    rgba[0] = fgetc(src);
    rgba[1] = fgetc(src);
    rgba[2] = fgetc(src);
    rgba[3] = fgetc(src);
    fseek(src, old_position, SEEK_SET);

    fprintf(stdout, "%d %d %d # %d\n", rgba[0], rgba[1], rgba[2], rgba[3]);
  }

  return 0;
}

int main(int argc, char **argv)
{
  FILE *src;

  src = fopen(argv[1], "rb");

  if (src)
  {
    read_agi(src);
    fclose(src);
  }
  else { fprintf(stderr, "Could not open input file.\n"); }

  return 0;
}
