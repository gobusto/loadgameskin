/**
@file
@brief A simple program for converting .APT image sets (used by Choro-Q games).

Tested with Seek & Destroy (Shin Combat Choro Q) and Penny Racers (Choro Q HG);
Road Trip Adventure (Choro Q HG 2) doesn't seem to use .APT images.

@todo This code basically works, but it needs tidying up.

Copyright (C) 2016 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define APT_4CC 0x00545041  /**< @brief Spells "APT" in ASCII */
#define APT_HEAD_SIZE 32
#define APT_ITEM_SIZE 32

static unsigned long read_ui32(FILE *src)
{
  unsigned long i = (unsigned long)fgetc(src);
  i += (unsigned long)fgetc(src) << 8;
  i += (unsigned long)fgetc(src) << 16;
  return i + ((unsigned long)fgetc(src) << 24);
}

int main(int argc, char **argv)
{
  FILE *src = NULL;
  long flen, data_offs;
  unsigned long magic_number, num_entries, i;

  if (argc < 2)
  {
    fprintf(stderr, "Usage: %s filename.bin\n", argv[0]);
    return 1;
  }

  src = fopen(argv[1], "rb");
  if (!src)
  {
    fprintf(stderr, "Couldn't open %s\n", argv[1]);
    return 1;
  }

  fseek(src, 0, SEEK_END);
  flen = ftell(src);
  rewind(src);

  fprintf(stderr, "%s (%ld bytes)\n", argv[1], flen);
  if (flen < APT_HEAD_SIZE + APT_ITEM_SIZE)
  {
    fprintf(stderr, "Invalid file length\n");
    goto CLEANUP;
  }

  /* The first 32 bytes are the file header: */
  magic_number = read_ui32(src);
  num_entries = read_ui32(src);

  fprintf(stderr, "\nHEADER:\n-------\n");
  fprintf(stderr, "MAGIC NUMBER: %08lx\n", magic_number);
  fprintf(stderr, "IMAGES COUNT: %lu\n", num_entries);
  fprintf(stderr, "????????????: %lu\n", read_ui32(src));
  fprintf(stderr, "????????????: %lu\n", read_ui32(src));
  fprintf(stderr, "????????????: %lu\n", read_ui32(src));
  fprintf(stderr, "????????????: %lu\n", read_ui32(src));
  fprintf(stderr, "????????????: %lu\n", read_ui32(src));
  fprintf(stderr, "????????????: %lu\n", read_ui32(src));

  if (magic_number != APT_4CC)
  {
    fprintf(stderr, "Invalid 4CC\n");
    goto CLEANUP;
  }

  fprintf(stderr, "\nIMAGES:\n-------\n");
  data_offs = APT_HEAD_SIZE + (num_entries * APT_ITEM_SIZE);
  for (i = 0; i < num_entries; ++i)
  {
    char name[32];
    unsigned long w, h, bpp, pal_size;

    fread(name, 1, 16, src);
    w = read_ui32(src);
    h = read_ui32(src);
    bpp = read_ui32(src);
    pal_size = read_ui32(src);

    fprintf(stderr, "%lu: %s - %lux%lu, %lubpp (%lu palette entries)\n", i, name, w, h, bpp, pal_size);

    if (bpp <= 24 && pal_size <= 256)
    {
      FILE *out;
      unsigned char pal[256 * 4];
      unsigned long a;
      long oldpos;

      oldpos = ftell(src);
      fseek(src, data_offs, SEEK_SET);

      if (bpp < 16) { fread(pal, 1, pal_size * 4, src); }

      strcat(name, ".pnm");
      out = fopen(name, "w");
      if (out) { fprintf(out, "P3 %lu %lu 255\n", w, h); }
      for (a = 0; a < w * h * bpp/8; ++a)
      {
        int val = fgetc(src);
        switch (bpp)
        {
          case 4:
          {
            int v1 = (val >> 0) & 0xF;
            int v2 = (val >> 4) & 0XF;
            fprintf(out, "%d %d %d\n", pal[v1*4+0], pal[v1*4+1], pal[v1*4+2]);
            fprintf(out, "%d %d %d\n", pal[v2*4+0], pal[v2*4+1], pal[v2*4+2]);
          }
          break;

          /* FIXME: This doesn't seem to be right; the colours look wrong... */
          case 8:
            fprintf(out, "%d %d %d\n", pal[val*4+0], pal[val*4+1], pal[val*4+2]);
          break;

          case 24:
            fprintf(out, "%d ", val);
          break;

          default:
          break;
        }
      }
      if (out) { fclose(out); }

      data_offs = ftell(src);
      fseek(src, oldpos, SEEK_SET);
    }
  }

  /* Close the file and end the program. */
  CLEANUP:
  fclose(src);
  return 0;
}
