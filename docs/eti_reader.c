/**
@file
@brief A simple program for converting Robot Warlords ETI files into PPM format

Copyright (C) 2016 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>

#define ETI_4CC 20
#define ETI_HEAD_SIZE 16

#define ETI_FLAG_PALETTE 0x00000001

unsigned short read_ui16(FILE *src)
{
  unsigned short i = (unsigned short)fgetc(src);
  i += (unsigned short)fgetc(src) << 8;
  return i;
}

unsigned long read_ui32(FILE *src)
{
  unsigned long i = (unsigned long)fgetc(src);
  i += (unsigned long)fgetc(src) << 8;
  i += (unsigned long)fgetc(src) << 16;
  i += (unsigned long)fgetc(src) << 24;
  return i;
}

void read_rgb(FILE *src, unsigned char *rgb)
{
  unsigned short value = read_ui16(src);
  rgb[0] = ((value >> 0 ) & 31) * 8;
  rgb[1] = ((value >> 5 ) & 31) * 8;
  rgb[2] = ((value >> 10) & 31) * 8;
}

int main(int argc, char **argv)
{
  FILE *src;
  long length, eof_offset;

  unsigned long flags, i;
  unsigned short w, h;

  src = fopen(argv[1], "rb");
  if (!src)
  {
    fprintf(stderr, "Could not open input file.\n");
    return 1;
  }

  fseek(src, 0, SEEK_END);
  length = ftell(src);
  rewind(src);

  if (length < ETI_HEAD_SIZE)
  {
    fprintf(stderr, "File is too short.\n");
    goto CLEANUP;
  }

  if (read_ui32(src) != ETI_4CC)
  {
    fprintf(stderr, "Invalid 4CC\n");
    goto CLEANUP;
  }

  /* NOTE: I'm not sure what all flags do yet... */
  flags = read_ui32(src);

  if (flags & ETI_FLAG_PALETTE)
  {
    unsigned long actual_image_offset, temp;

    /* The first "image" is actually the palette; the real data comes later: */
    actual_image_offset = read_ui32(src);

    /* The palette always(?) contains 256 16-bit values: */
    if (actual_image_offset != 528)
    {
      fprintf(stderr, "Unexpected image offset: %lu\n", actual_image_offset);
      goto CLEANUP;
    }

    /* The palette always(?) pretends to be a 16x16 image: */
    w = read_ui16(src);
    h = read_ui16(src);
    if (w != 16 || h != 16)
    {
      fprintf(stderr, "Unexpected palette size: %dx%d\n", w, h);
      goto CLEANUP;
    }

    /* Skip past the palette "image" data: */
    fseek(src, (long)actual_image_offset, SEEK_SET);

    /* The second, "fake" header has no 4CC or flags: */
    if ((temp = read_ui32(src)) != 0)
    {
      fprintf(stderr, "Unexpected pseudo-4CC: %lu\n", temp);
      goto CLEANUP;
    }
    if ((temp = read_ui32(src)) != 0)
    {
      fprintf(stderr, "Unexpected pseudo-flags: %lu\n", temp);
      goto CLEANUP;
    }
  }

  /* This might be a few bytes short if the file is padded for some reason: */
  eof_offset = read_ui32(src);

  if (eof_offset > length)
  {
    fprintf(stderr, "End-of-File offset is too large: %lu\n", eof_offset);
    goto CLEANUP;
  }

  /* Next come the image dimensions: */
  w = read_ui16(src);
  h = read_ui16(src);

  /* The size of the data will vary based on whether a palette is used: */
  if (ftell(src) + (w * h * ((flags & ETI_FLAG_PALETTE) ? 1 : 2)) > length)
  {
    fprintf(stderr, "Image dimensions too large for file size: %dx%d\n", w, h);
    goto CLEANUP;
  }

  fprintf(stdout, "P3 %d %d 255\n", w, h);

  /* Next comes the pixel data: */
  for (i = 0; i < w*h; ++i)
  {
    unsigned char rgbi[4];

    if (flags & ETI_FLAG_PALETTE)
    {
      long oldpos;
      unsigned char temp;
      rgbi[3] = fgetc(src);

      /*
      If we divide the palette into blocks of 32 RGBA values, then the first and
      last 8 colours can be used as-is. However, the second and third blocks (of
      8 colours) must be swapped. In other words, if the original palette looked
      like this:

          0  1  2  3  4  5  6  7
          8  9  10 11 12 13 14 15
          16 17 18 19 20 21 22 23
          24 25 26 27 28 29 30 31

      ...then we need to use it like this:

          0  1  2  3  4  5  6  7
          16 17 18 19 20 21 22 23
          8  9  10 11 12 13 14 15
          24 25 26 27 28 29 30 31

      A quick internet search seems to indicate that this is a common thing for
      PS2 games using 256-colour palettes, so this isn't specific to this game.
      */

      temp = rgbi[3] % 32;
      if (temp >= 8  && temp < 16) { rgbi[3] += 8; }
      if (temp >= 16 && temp < 24) { rgbi[3] -= 8; }

      oldpos = ftell(src);
      fseek(src, ETI_HEAD_SIZE + (rgbi[3] * 2), SEEK_SET);
      read_rgb(src, rgbi);
      fseek(src, oldpos, SEEK_SET);
    }
    else
    {
      rgbi[3] = 0;
      read_rgb(src, rgbi);
    }
    fprintf(stdout, "%d %d %d # %d\n", rgbi[0], rgbi[1], rgbi[2], rgbi[3]);
  }

  /* Close any open files and end the program: */
  CLEANUP:
  fclose(src);
  return 0;
}
